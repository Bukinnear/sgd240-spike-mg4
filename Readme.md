---
title: Spike Report
---

Optimisation
============

Introduction
------------

One of the hardest questions in Network Programming is how to reduce
latency and reduce bandwidth usage.

We want to learn what methods we can use within Unreal Engine to do so.

Goals
-----

-   The Spike Report should answer each of the Gap questions:

    -   Tech: How does Unreal Engine allow us to profile network
        traffic?

    -   Skill: What can we do to reduce network traffic?

    -   Knowledge: How can we reduce the effects of high latency on
        players?

Building on Spike MG-3

1.  [Profile](https://docs.unrealengine.com/latest/INT/Gameplay/Tools/NetworkProfiler/index.html)
    the game as-is

2.  [Optimise](https://docs.unrealengine.com/latest/INT/Gameplay/Networking/Actors/ReplicationPerformance/index.html)
    it

3.  [Profile](https://docs.unrealengine.com/latest/INT/Gameplay/Tools/NetworkProfiler/index.html)
    it again

4.  Go to step 2.

Personnel
---------

In this section, list the primary author of the spike report, as well as
any personnel who assisted in completing the work.

  ------------------- -----------------
  Primary – Jared K   Secondary – N/A
  ------------------- -----------------

Technologies, Tools, and Resources used
---------------------------------------

-   Network Profiler UE4 docs
    <https://docs.unrealengine.com/latest/INT/Gameplay/Tools/NetworkProfiler/index.html>

-   Network Profiler update (possibly useful)
    <https://trello.com/c/V469hYKd/255-done-411-network-profiler-improvements>

-   Enabling profiling via Console
    <https://answers.unrealengine.com/questions/53219/network-how-to-debugshow-network-traffic-in-ue4.html>

Tasks Undertaken
----------------

### Profiling the Game

1.  To use the profiler, we need to generate data to analyze. To do
    this, we can run the console command “netprofile”

2.  Once we have a collection of data, we can open the profiler tool
    found at C:\\Program Files\\Epic
    Games\\UE\_4.15\\Engine\\Binaries\\DotNET\\NetworkProfiler.exe (or
    wherever you have your Unreal installation)

3.  The saved profile data can be found at
    YourProjectName\\Saved\\Profiling

4.  Analyze the data to identify where your code is slowest. Or what may
    be using more resources than expected.

What we found out
-----------------

### Optimizing the Game

1.  This is an extremely broad subject that cannot be taught in a single
    document. But the general idea is to identify areas in your code
    that are the greatest burden on bandwidth and update time, and
    attempt to reduce its’ load by either refactoring it, or other by
    using other methods.

    a.  These methods may include reducing how often an object updates
        to the network, or by reducing its’ network relevancy to avoid
        unnecessary updates when the object is not relevant to players.

### Why Didn’t I optimize?

1.  There was no code in my project that required it.

    a.  The primary reason for this, is that the code was designed with
        the idea of avoiding unnecessary updates or activity, to prevent
        undue load on the network (and other resources)

    b.  For example, UI elements are only updated when they detect a
        change in the relevant values, instead of on every frame.

2.  This was also a very small project that didn’t have much gameplay
    complexity involved. Very few elements were replicated, so there
    wasn’t much load on the game to begin with

 \[Optional\] Open Issues/risks
-------------------------------

This will need to be revisited in future projects, as the level of
optimization required varies greatly from project to project, and vary
rarely are two the same

 \[Optional\] Recommendations
-----------------------------
